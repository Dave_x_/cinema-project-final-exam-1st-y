
package com.mycompany.parcialfinal;

import java.util.Scanner;

public class ParcialFinal {
    static Scanner sc = new Scanner(System.in);
    static int matriz[][] = new int[8][20];
    static int categoria1, categoria2, categoria3;

    public static void main(String[] args) {
        menu();
    }

    public static void menu() {
        int menu = 0;
        do {
            System.out.println("Por favor seleccione una opción: ");
            System.out.println("1. Mostrar plano de la sala");
            System.out.println("2. Vender un boleto");
            System.out.println("3. Cambiar silla");
            System.out.println("4. Mostrar ingresos por venta");
            System.out.println("5. Consultar sillas ocupadas de toda la sala");
            System.out.println("6. Salir");
            
            menu = sc.nextInt();
            
            switch (menu) {
                case 1:
                    System.out.println("Mostrar plano de la sala");
                    plano();
                    break;
                case 2:
                    venta();
                    break;
                case 3:
                    System.out.println("Cambiar silla");
                    cambiarSilla();
                    break;
                case 4:
                    System.out.println("Mostrar ingresos por venta");
                    ingresos();
                    break;
                case 5:
                    System.out.println("Consultar sillas ocupadas de toda la sala");
                    ocupacion();
                    break;
                case 6:
                    System.out.println("Fin del programa");
                    break;
                default:
                    System.out.println("Opción no válida. Por favor, seleccione nuevamente.");
                    break;
            }
        } while (menu != 6);
    }

    public static void plano() {
        System.out.println("Puestos en el orden:");
        System.out.println("i = Fila, j = Columna, X = Ocupado");

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 20; j++) {
                System.out.print("A" + (i + 1) + "-" + (j + 1) + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void venta() {
      System.out.println("El valor del boleto es: \n De la fila 1 a la 2: 10000 pesos. \n De la fila 3 a la 5: 20000 pesos. \n De la fila 6 la 8: 30000 pesos.");
    System.out.println("Ingrese la Silla en el formato Ai-j, por favor coloque solo números de la forma \ni=Fila\nj=columna \n");
    int filas, columnas;
   
        do {
            System.out.println("Tenga cuidado, se va a repetir el menú si no selecciona la opción correcta");
            System.out.println("Ingrese el número de fila (i): ");
            filas = sc.nextInt();
            System.out.println("Ingrese el número de columna (j)");
            columnas = sc.nextInt();
            matriz= new int[filas][columnas];
            System.out.println("Usted escogió el puesto A"+filas+"-"+columnas);
            System.out.println("\n");
        } while (filas<0||filas>9 || columnas<0||columnas>21);
        
        if (filas>=1 && filas<=2) {
        System.out.println("El valor del boleto en esta categoría es de 10000\n");    
        categoria3++;
        } else if (filas>=3 && filas<=5) {
        System.out.println("El valor el valor del boleto en esta categoría es de 20000\n");    
        categoria2++;
        } else  if (filas>=6 && filas<=8) {
        System.out.println("El valor del boleto en esta categoría es de 30000\n");    
        categoria1++;
        }

    }

    public static void cambiarSilla() {
        System.out.println("Ingrese la silla que desea cambiar en el formato Ai-j:");
        int filaActual, columnaActual, nuevaFila, nuevaColumna;

        System.out.println("Ingrese el número de fila actual (i): ");
        filaActual = sc.nextInt();
        System.out.println("Ingrese el número de columna actual (j):");
        columnaActual = sc.nextInt();

        if (filaActual >= 1 && filaActual <= 8 && columnaActual >= 1 && columnaActual <= 20) {
            System.out.println("Ingrese el número de la nueva fila (i): ");
            nuevaFila = sc.nextInt();
            System.out.println("Ingrese el número de la nueva columna (j):");
            nuevaColumna = sc.nextInt();

            if (nuevaFila >= 1 && nuevaFila <= 8 && nuevaColumna >= 1 && nuevaColumna <= 20) {
                System.out.println("Silla A" + filaActual + "-" + columnaActual + " cambiada a A" + nuevaFila + "-" + nuevaColumna);
            } else {
                System.out.println("La nueva posición no es válida.");
            }
        } else {
            System.out.println("La silla actual no es válida.");
        }
    }

    public static void ingresos() {
        int total_Categoria1 = categoria1 * 30000;
        int total_Categoria2 = categoria2 * 20000;
        int total_Categoria3 = categoria3 * 10000;

        System.out.println("Mostrar ingresos por ventas:");
        System.out.println("Total: " + (total_Categoria1 + total_Categoria2 + total_Categoria3) + " COP");
        System.out.println();
    }

    public static void ocupacion() {
        System.out.println("La cantidad de sillas ocupadas de toda la sala es:");
        System.out.println("Categoría 1: " + categoria1);
        System.out.println("Categoría 2: " + categoria2);
        System.out.println("Categoría 3: " + categoria3);
        System.out.println("Total sillas usadas: " + (categoria1 + categoria2 + categoria3));
        System.out.println();
    }
}
